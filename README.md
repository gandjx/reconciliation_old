# INSTALLATION DU MODULE RECONCILIATION
# =====================================

**Copier le répertoire reconciliation dans modules.**

**Copier les fichiers et répertoires suivants dans le répertoire custom**

    modules/templates/datasources
    modules/templates/datasources/letterbox_attachment.php
    core/xml
    core/xml/actions_pages.xml
    apps/maarch_entreprise/definition_mail_categories.php
    apps/maarch_entreprise/xml/IVS
    apps/maarch_entreprise/js

**Modifier le fichier apps/maarch_entreprise/xml/config.xml**

Ajout de la catégorie suivante:

	<category>
		<id>attachment</id>
		<label>_ATTACHMENT</label>
	</category>
            
Ajout du module suivant:

	<MODULES>
		<moduleid>reconciliation</moduleid>
		<comment>_RECONCILIATION_COMMENT</comment>
	</MODULES>
	
**Modifier le fichier IVS/data_types.xml**

Ajout de l'énumération suivante : 
	
	<dataType name="category_list" base="string">
      <enumeration value="incoming" />
      <enumeration value="outgoing" />
      <enumeration value="internal" />
      <enumeration value="attachment" />	<-- Ligne à rajouter
    </dataType>
    
**Modifier le fichier apps/maarch_entreprise/definitions_mail_categories.php**

Ajout du tableau de la catégorie attachment

	///////////////////////////// RECONCILIATION ////////////////////////////////////////////////
	$_ENV['categories']['attachment'] = array ();
	$_ENV['categories']['attachment']['img_cat'] = '<i class="fa fa-paperclip fa-2x"></i>';
	$_ENV['categories']['attachment']['other_cases'] = array ();
	$_ENV['categories']['attachment']['other_cases']['chrono_number'] = array (
		'type_form' => 'integer',
		'type_field' => 'integer',
		'mandatory' => false,
		'label' => _CHRONO_NUMBER,
		'table' => 'none',
		'img' => 'compass',
		'modify' => false,
		'form_show' => 'textfield'
	);
	$_ENV['categories']['attachment']['other_cases']['contact'] = array (
		'type_form' => 'string',
		'type_field' => 'string',
		'mandatory' => true,
		'display' => 'none',
		'label' => _DEST,
		'table' => 'coll_ext',
		'img' => 'book',
		'special' => 'dest_user_id,dest_contact_id',
		'modify' => false
	);
	$_ENV['categories']['attachment']['type_id'] = array (
		'type_form' => 'integer',
		'type_field' => 'integer',
		'mandatory' => true,
		'label' => _DOCTYPE_MAIL,
		'table' => 'res',
		'img' => 'file',
		'modify' => true,
		'form_show' => 'select'
	);
	
**Modifier le fichier js/indexing.js**

Affichage des éléments de liaison de fichier (à la fin de la fonction change_category)

	if(cat_id == 'attachment'){
        document.getElementById("attachment_tr").style.display='table-row';
        document.getElementById("attach_show").style.display='table-row';
    }
    
Ajout du tableau contenant les champs à afficher pour la catégorie attachment (fonction change_category)
	
	else if(cat_id == 'attachment'){
        var category = [
            {id:'doctype_mail', type:'label', state:'display'},
            {id:'doctype_res', type:'label', state:'hide'},
            {id:'priority_tr', type:'tr', state:'hide'},
            {id:'doc_date_label', type:'label', state:'hide'},
            {id:'doc_date_tr', type:'label', state:'hide'},
            {id:'mail_date_label', type:'label', state:'hide'},
            {id:'author_tr', type:'tr', state:'hide'},
            {id:'admission_date_tr', type:'tr', state:'hide'},
            {id:'contact_check', type:'tr', state:'hide'},
            {id:'nature_id_tr', type:'tr', state:'hide'},
            {id:'department_tr', type:'tr', state:'hide'},
            {id:'label_dep_dest', type:'label', state:'hide'},
            {id:'label_dep_exp', type:'label', state:'hide'},
            {id:'process_limit_date_use_tr', type:'tr', state:'hide'},
            {id:'process_limit_date_tr', type:'tr', state:'hide'},
            {id:'box_id_tr', type:'tr', state:'hide'},
            {id:'confidentiality_tr', type:'tr', state:'hide'},
            {id:'contact_choose_tr', type:'tr', state:'hide'},
            {id:'contact_choose_2_tr', type:'tr', state:'hide'},
            {id:'contact_choose_3_tr', type:'tr', state:'hide'},
            {id:'dest_contact_choose_label', type:'label', state:'hide'},
            {id:'exp_contact_choose_label', type:'label', state:'hide'},
            {id:'contact_id_tr', type:'tr', state:'hide'},
            {id:'dest_contact', type:'label', state:'hide'},
            {id:'exp_contact', type:'label', state:'hide'},
            {id:'type_multi_contact_external_icon', type:'label', state:'hide'},
            {id:'type_contact_internal', type:'radiobutton', state:typeContactInternal},
            {id:'type_contact_external', type:'radiobutton', state:typeContactExternal},
            {id:'type_multi_contact_external', type:'radiobutton', state:typeMultiContactExternal},
            {id:'folder_tr', type:'tr', state:'hide'},
            {id:'category_id_mandatory', type:'label', state:'hide'},
            {id:'type_id_mandatory', type:'label', state:'hide'},
            {id:'type_id_tr', type:'tr', state:'hide'},
            {id:'diff_list_tr', type:'tr', state:'hide'},
            {id:'priority_mandatory', type:'label', state:'hide'},
            {id:'doc_date_mandatory', type:'label', state:'hide'},
            {id:'author_mandatory', type:'label', state:'hide'},
            {id:'admission_date_mandatory', type:'label', state:'hide'},
            {id:'type_contact_mandatory', type:'label', state:'hide'},
            {id:'contact_mandatory', type:'label', state:'hide'},
            {id:'nature_id_mandatory', type:'label', state:'hide'},
            {id:'subject_mandatory', type:'label', state:'hide'},
            //{id:'subject_tr', type:'label', state:'hide'},
            {id:'destination_mandatory', type:'label', state:'hide'},
            {id:'process_limit_date_use_mandatory', type:'label', state:'hide'},
            {id:'process_limit_date_mandatory', type:'label', state:'hide'},
            {id:'chrono_number', type:'label', state:'hide'},
            {id:'chrono_number_tr', type:'tr', state:'hide'},
            {id:'chrono_number_mandatory', type:'label', state:'hide'},
            {id:'folder_mandatory', type:'label', state:'hide'},
            {id:'res_id_link', type:'label', state:'hide'},
            {id:'status', type:'tr', state:'hide'},
            {id:'add_multi_contact_tr', type:'tr', state:'hide'},
            {id:'show_multi_contact_tr', type:'tr', state:'hide'}
        ];
    }
    
Suppression à l'écran de certains champs inutiles au module Reconciliation

	if(cat_id == 'ged_doc'){
        document.getElementById("diff_list_tr").style.display = 'none';
    }else if(cat_id == 'attachment'){
        document.getElementById("subject_tr").style.display = 'none';
        document.getElementById("diff_list_tr").style.display = 'none';
    }else{
        document.getElementById("diff_list_tr").style.display = 'table-row';
        document.getElementById("subject_tr").style.display = 'table-row';
    }
    
Modification de la fonction change_contact_type 

	var cat_id = $(category_id).options[$(category_id).selectedIndex].value;
	if(cat_id != 'attachment') Element.setStyle(contact_id_tr, {display : 'table-row'});

**Modifier le fichier core/xml/action_pages.xml**

Rajout de la page d'action Reconciliation 
	
	<ACTIONPAGE>
        <ID>reconciliation</ID>
        <LABEL>Réconciliation</LABEL>
        <NAME>reconciliation</NAME>
        <ORIGIN>module</ORIGIN>
        <MODULE>reconciliation</MODULE>
        <FLAG_CREATE>false</FLAG_CREATE>
        <COLLECTIONS>
            <COLL_ID>letterbox_coll</COLL_ID>
        </COLLECTIONS>
    </ACTIONPAGE>

# ----------------------------------------------

**Coté fonctionnel**

    Créer une nouvelle action
    Description : Rattacher une réponse à un courrier
    Statut associé : Supprimé
    Page de résultat de l'action : Réconciliation
    Mot clé (paramètres système): Indexation
    Tracer l'action : Oui
    Action de dossier : Non
    Choisissez une ou plusieurs catégories associées : 
    (Si aucune catégorie sélectionnée alors l'action est valable pour toute les catégories)
    Aucune

**Créer une nouvelle corbeille**

    Collection : Collection des courriers
    Identifiant : AttBasket
    Corbeille : Courriers à rapprocher
    Description : Corbeille des courriers à rapprocher
    Vue sur la table : status = 'PJQUAL'

**Ajouter un groupe**

    Liste de résultats: Liste avec filtre et réponses
    Action par défaut sur la ligne; Rattacher une réponse à un courrier

**Editer un nouveau modèle et insérer une nouvelle image avec les dimensions voulues et mettre dans la description de cette dernière**
    
    [attachments.chronoBarCode;ope=changepic;tagpos=inside;adjust;unique]


# Installation du watcher
# =======================

    cd /opt/maarch
    git clone https://github.com/splitbrain/Watcher.git
    sudo apt-get install python python-pyinotify

**Création du service**

    sudo vi /etc/systemd/system/watcher.service
    [Unit]
    Description=Files watcher for maarch 
    After=apache2.service

    [Service]
    User=edissyum
    ExecStart=/opt/maarch/Watcher/watcher.py -c /opt/maarch/Watcher/watcher.ini start
    ExecStop=/opt/maarch/Watcher/watcher.py stop
    ExecRestart=/opt/maarch/Watcher/watcher.py -c /opt/maarch/Watcher/watcher.ini restart

    [Install]
    WantedBy=default.target

    sudo systemctl start watcher
    sudo systemctl enable watcher

**Installation de paquets supplémentaires**

    sudo apt install python-lxml
    sudo pip install beautifulsoup4


