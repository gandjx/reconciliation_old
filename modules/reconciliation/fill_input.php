<!--
*
*
*   Fill all the reconciliation input with chrono_number, dest and title informations
*
*   @author : Nathan Cheval - NCH01
*
-->
<?php
$core = new core_tools();
$core->test_user();
$db = new Database();

// Get informations from config.xml
$multiple_res_id = $_SESSION['modules_loaded']['reconciliation']['multiple_res_id'];
$attach_to_empty = $_SESSION['modules_loaded']['reconciliation']['attach_to_empty'];
$attachmentTable = $_SESSION['tablename']['reconciliation']['attachment'];
$letterboxTable = $_SESSION['tablename']['reconciliation']['letterbox'];
$contactsV2Table = $_SESSION['tablename']['reconciliation']['contacts_v2'];
$contactsAddressesTable = $_SESSION['tablename']['reconciliation']['contact_addresses'];

// If no documents are choosen, force the user to modify his choice by going back in the modal window
// If multiple documents are choose, the user have to enter manually the chrono number, the title and the recipient
// If the possiblity to choose multiple document is 'false', force the user to modify his choice by going back in the modal window
if(count($_GET['field']) == 0){
    ?>
    <script type="text/javascript">
        if(confirm("<?php echo _ERROR_EMPTY_RES_ID ;?>")){
            history.back();
        }else history.back();
    </script>
    <?php
    exit();
}else if (count($_GET['field']) == 1) $nbResId = 1;
else if(count($_GET['field']) != 1 && $multiple_res_id == 'false'){
    ?>
    <script type="text/javascript">
        if(confirm("<?php echo _MULTIPLE_RES_ID_ERROR ;?>")){
            history.back();
        }else history.back();
    </script>
    <?php
    exit();
}else if(count($_GET['field']) != 1 && $multiple_res_id == 'true') $nbResId = count($_GET['field']);

// Check if at least one of the choosen documents isn't the current document
for ($i = 0; $i < count($_GET['field']); $i++){
    if($_GET['field'][$i] == $_SESSION['doc_id']){
        ?>
        <script type="text/javascript">
            if(confirm("<?php echo _SAME_RES_ID_ERROR;?>")){
                history.back();
            }else history.back();
        </script>
        <?php
        exit();
    }
}

//If there is one res_id, we get the recipient information, the chrono number and the title
if($nbResId == 1) {

    // Check if there is a response project
    $queryCheck = "SELECT * FROM " . $attachmentTable . " WHERE res_id_master = ? AND attachment_type = 'response_project' AND status <> 'DEL'";
    $arrayCheckInfos = array($_GET['field']);
    $stmtCheck = $db->query($queryCheck, $arrayCheckInfos);
    $queryCheck = $stmtCheck->fetchAll();

    // If the selected document doesn't have a response project and attach_to_empty parameter is false, send the user back in the modal to select a document with a response project
    if (!$queryCheck && $attach_to_empty == 'true') {
        $errorChrono = _NO_RESPONSE_PROJECT;
        $errorDest = _NO_RESPONSE_PROJECT;
    } else if (!$queryCheck && $attach_to_empty == 'false') {
        ?>
        <script type="text/javascript">
            if (confirm("<?php echo _ATTACH_TO_EMPTY_ERROR;?>")) {
                history.back();
            } else history.back();
        </script>
        <?php
        exit();
    } else if(count($queryCheck) > 1){
        $errorChrono = _MULTIPLE_RESPONSE_PROJECT_CHRONO;
        $errorDest = _MULTIPLE_RESPONSE_PROJECT_DEST;
    }

    // If there is one response project, we get the informations of this one
    if(count($queryCheck) == 1){
        // Retrieve the informations of the project response
        $queryProject = "SELECT identifier, title, dest_contact_id, dest_address_id FROM " . $attachmentTable . " WHERE res_id_master = ? AND attachment_type = 'response_project' AND status <> 'DEL'";
        $arrayProjectInfos = array($_SESSION['stockCheckbox']);
        $stmtProject = $db -> query($queryProject,$arrayProjectInfos);
        $queryResponse = $stmtProject -> fetchObject();

        // Retrieve the recipient informations
        $queryContact = "SELECT contact_addresses.firstname as firstNameAddress, 
                            contact_addresses.lastname as lastNameAddress,
                            contacts_v2.firstname as firstNameContact, 
                            contacts_v2.lastname as lastNameContact, 
                            address_num, 
                            address_street, 
                            address_complement, 
                            address_town, 
                            address_postal_code, 
                            address_country 
                      FROM " . $contactsAddressesTable . "
                      INNER JOIN " . $contactsV2Table . "
                      ON contact_addresses.contact_id = contacts_v2.contact_id 
                      WHERE contacts_v2.contact_id = ? 
                      AND contact_addresses.id = ?;";
        $arrayContactInfos = array($queryResponse -> dest_contact_id, $queryResponse -> dest_address_id);
        $stmtContact = $db -> query($queryContact,$arrayContactInfos);
        $queryContact = $stmtContact->fetchObject();

        // Create the last name, first name and adresses variables
        $tabContact = array();
        foreach($queryContact as $key => $value){
            if($value != ''){
                $tabContact[$key] = $value;
            }
        }

        // Create the variable to fill the recipient input
        if($tabContact['firstnameaddress'] == '') $contactInfos .= $tabContact['firstnamecontact'] . " " . $tabContact['lastnamecontact'];
        else if ($tabContact['firstnamecontact'] == '') $contactInfos .= $tabContact['firstnameaddress'] . " " . $tabContact['lastnameaddress'];
        else if ($tabContact['firstnameaddress'] != '' && $tabContact['firstnamecontact'] != '') $contactInfos .= $tabContact['firstnamecontact'] . "" . $tabContact['lastnamecontact'];
        $contactInfos .= " - " . $tabContact['address_num'] . " " . $tabContact['address_street'] . " " . $tabContact['address_town'] . " " . $tabContact['address_postal_code'] . " " . $tabContact['address_country'];
    }
}else{
    // Check if one of the selected document own a response projet, if attach_to_empty parameter is false
    if($attach_to_empty == 'false'){
        for($i =0; $i < count($_GET['field']); $i++){
            $queryCheck = "SELECT * FROM " . $attachmentTable . " WHERE res_id_master = ? AND attachment_type = 'response_project'";
            $arrayCheckInfos = array($_GET['field'][$i]);
            $stmtCheck = $db->query($queryCheck, $arrayCheckInfos);
            $queryCheck = $stmtCheck->fetchObject();
            if(!$queryCheck){
                ?>
                <script type="text/javascript">
                    if (confirm("<?php echo _ATTACH_TO_EMPTY_ERROR;?>")) {
                        history.back();
                    } else history.back();
                </script>
                <?php
                exit();
            }
        }
    }
    $errorChrono = _MULTIPLE_RES_ID_CHRONO;
    $errorDest = _MULTIPLE_RES_ID_DEST;
}

// Get the informations of the current document in case there is more than one response project
$queryDefault = "SELECT subject FROM " . $letterboxTable . " WHERE res_id = ?";
$arrayDefaultInfos = array($_SESSION['doc_id']);
$stmt1 = $db->query($queryDefault, $arrayDefaultInfos);
$defaultInfos = $stmt1->fetchObject();

// Display and hide all the necessary informations to the new categorie Attachment
?>
<script type="text/javascript">
    var title_tr = window.opener.$('title_tr');
    var chrono_tr = window.opener.$('chrono_number_tr');
    var chrono_res = window.opener.$('chrono_number');
    var chrono_check = window.opener.$('chrono_check');
    var contact_check = window.opener.$('contact_check');
    var contact_tr = window.opener.$('contact_id_tr');
    var dest_contact = window.opener.$('dest_contact');
    var create_contact = window.opener.$('create_contact');
    var author_contact = window.opener.$('author_contact');
    var contact_icon = window.opener.$('type_contact_external_icon');
    var contact_card = window.opener.$('contact_card');

    title_tr.style.display = 'table-row';
    chrono_tr.style.display = 'table-row';
    chrono_res.style.display = 'table-row';
    chrono_check.style.display = 'table-row';
    contact_check.style.display = 'table-row';
    contact_tr.style.display = 'table-row';
    dest_contact.style.display = 'table-row';
    create_contact.style.display = 'none';
    contact_icon.style.display = 'none';
    author_contact.style.display = 'none';
    contact_card.style.display = 'none';
</script>
<?php
// If there is only one response project, the chrono number, title and recipient are automatically fill
if(isset($stmtProject ) && $stmtProject -> rowCount() == 1) {
    ?>
    <script type="text/javascript">
        var chrono_res = window.opener.$('chrono_number');
        var title_res = window.opener.$('title');
        var chrono_check = window.opener.$('chrono_check');
        var contact = window.opener.$('contact');
        var contact_check = window.opener.$("contact_check");
        var contact_id = window.opener.$('contactid');
        var address_id = window.opener.$('addressid');

        chrono_check.style.display = 'none';
        contact_check.style.display = 'none';

        if (chrono_res && title_res){
            chrono_res.value = <?php echo json_encode($queryResponse -> identifier);?>;
            chrono_res.setAttribute('readonly','readonly');
            chrono_res.setAttribute('class','readonly');
            title_res.value = <?php echo json_encode($queryResponse -> title);?>;
            contact.value = <?php echo json_encode($contactInfos);?>;
            contact_id.value = <?php echo json_encode($queryResponse -> dest_contact_id);?>;
            address_id.value = <?php echo json_encode($queryResponse -> dest_address_id);?>;
        }
    </script>
    <?php
}else { // If there is more than one response project or multiple document selected,the user have to enter manually the chrono number and the recipient informations. The title is automatically fill with the current document title
    ?>
    <script type="text/javascript">
        var chrono_res = window.opener.$('chrono_number');
        var title_res = window.opener.$('title');
        var contact= window.opener.$('contact');

        var chrono_check = window.opener.$("chrono_check");
        var contact_check = window.opener.$("contact_check");

        if (chrono_res && title_res && chrono_check) {
            chrono_res.value = '';
            contact.value = '';
            chrono_res.removeAttribute('readonly');
            chrono_res.removeAttribute('class');
            title_res.value = <?php echo json_encode($defaultInfos -> subject);?>;
            chrono_check.innerHTML = "<td colspan=\"3\" style=\"font-size: 9px;text-align: center;color:#ea0000;\"><?php echo $errorChrono ?></td>";
            contact_check.innerHTML = "<td colspan=\"3\" style=\"font-size: 9px;text-align: center;color:#ea0000;\"><?php echo $errorDest; ?> </td>";
        }
    </script>
    <?php
}
?>

<script type="text/javascript">
    var input_res = window.opener.$('res_id');
    if (input_res) {
        input_res.value=<?php echo json_encode($_GET['field']);?>;
    }
    self.close();
</script>