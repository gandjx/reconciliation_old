<?php

/*
*
*   Part of Reconciliation module
*   Allow to add the incoming response project as an attachment
*
*   @author : Nathan Cheval - NCH01
*
*/

require_once 'modules/reconciliation/class/reconciliation_controler.php';

$core = new core_tools();
$core->test_user();
$db = new Database();
$reconciliationControler = new reconciliation_controler();

$letterboxTable = $_SESSION['tablename']['reconciliation']['letterbox'];

// Retrieve the parent res_id (the document which receive the attachment) and the res_id of the attachment we will inject
$parentResId = $_SESSION['stockCheckbox'];
$childResId = $_SESSION['doc_id'];

// Retrieve the data of the form (title, chrono number, recipient etc...)
$formValues = $reconciliationControler -> get_values_in_array($_REQUEST['form_values']);
$tabFormValues = array();
foreach($formValues as $tmpTab){
    if($tmpTab['ID'] == 'title' || $tmpTab['ID'] == 'chrono_number' || $tmpTab['ID'] == 'contactid' || $tmpTab['ID'] == 'addressid'){
    	if(trim($tmpTab['VALUE']) != '') // Case of some empty value, that cause some errors
        	$tabFormValues[$tmpTab['ID']] = $tmpTab['VALUE'];
    }
}

$_SESSION['modules_loaded']['reconciliation']['tabFormValues'] = $tabFormValues;

// Retrieve the informations of the newly scanned document (the one to attach as an attachment)
$queryChildInfos = "SELECT * FROM " . $letterboxTable . " WHERE res_id = ?";
$arrayChildInfos = array($childResId);
$stmt = $db -> query($queryChildInfos,$arrayChildInfos);
$data = array();
foreach ($stmt -> fetchObject() as $key => $value){
    if($value != ''
        && $key != 'modification_date'
        && $key != 'is_frozen'
        && $key != 'tablename'
        && $key != 'locker_user_id'
        && $key != 'locker_time'
        && $key != 'confidentiality') {
        $data[$key] = $value;
    }
}

// Retrieve the PATH TEMPLATE
$queryPath = "SELECT path_template FROM docservers WHERE docserver_id = ?";
$arrayPathInfos = array($data['docserver_id']);
$stmt2 = $db -> query($queryPath,$arrayPathInfos);

$data['path_template'] = $stmt2 -> fetchColumn();

// The column 'relation' need to be set at 1. Otherwise, the suppression of the attachment isn't possible
$data['relation'] = 1;

// The status need to be TRA
$data['status'] = 'TRA';

// The attachment type need to be signed_response --> maybe it will change
$data['attachment_type'] = 'signed_response';

// The title is retrieve from the validate page
$data['subject'] = $tabFormValues['title'];

// Same for chrono number
if(isset($tabFormValues['chrono_number']))
	$data['identifier'] = $tabFormValues['chrono_number'];

// Same for recipient informations
if(isset($tabFormValues['addressid']))
	$data['dest_address_id'] = $tabFormValues['addressid'];
if(is_numeric($tabFormValues['contactid']))
	$data['dest_contact_id'] = $tabFormValues['contactid'];

// res_attachment insertion
if(count($parentResId) == 1 ) {
	$insertResAttach = $reconciliationControler->addToResAttachments($parentResId[0], 'letterbox_coll', $data);
}else
    for($i = 0; $i <= count($parentResId); $i++){
        $insertResAttach = $reconciliationControler -> addToResAttachments($parentResId[$i], 'letterbox_coll', $data);
    }