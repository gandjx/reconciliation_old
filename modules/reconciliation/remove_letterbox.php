<?php
/*
*
*   Part of Reconciliation module
*   Mark the incoming document as delete in res_letterbox table. Also the response project if necessary
*
*   @author : Nathan Cheval - NCH01
*/

$core = new core_tools();
$core->test_user();
$db = new Database();

// Variable declaration
$res_id = $_SESSION['doc_id'];
$res_id_master = $_SESSION['stockCheckbox'];
$letterboxTable = $_SESSION['tablename']['reconciliation']['letterbox'];
$attachmentTable = $_SESSION['tablename']['reconciliation']['attachment'];
$delete_response_project = $_SESSION['modules_loaded']['reconciliation']['delete_response_project'];

// Modification of the incoming document, as deleted
$queryDel = "UPDATE " . $letterboxTable . " SET status = 'DEL' WHERE res_id = ?";
$arrayDelInfos = array($res_id);
$stmtDel = $db -> query($queryDel,$arrayDelInfos);

// Deletion of the response project, with his chrono number and the res_id_master
if($delete_response_project == 'true'){
    $tabFormValues = $_SESSION['modules_loaded']['reconciliation']['tabFormValues'];
    $chronoNumber = $tabFormValues['chrono_number'];

    $queryDelProject = "UPDATE " . $attachmentTable . " SET status = 'DEL' WHERE res_id_master = ? AND identifier = ? AND status NOT IN ('DEL','TMP') AND attachment_type = 'response_project'";
    $arrayDelProjectInfos = array($res_id_master[0],$chronoNumber);

    $stmtDelProject = $db -> query($queryDelProject, $arrayDelProjectInfos);
}