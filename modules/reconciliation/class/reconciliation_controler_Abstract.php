<?php

abstract class reconciliation_controler_abstract{
    /*
     * Fonction pour ajouter une nouvelle entrée dans la table res_attachments
     * Fork de la fonction storeAttachmentResource de la class attachments_controler_abstract
     */
    public function addToResAttachments($resId, $collId, $data){
        require_once 'core/class/class_db_pdo.php';
        require_once 'core/class/class_request.php';
        require_once 'core/class/class_resource.php';
        require_once 'core/class/docservers_controler.php';
        require_once 'core/class/class_security.php';

        $sec = new security();
        $table = $sec->retrieve_table_from_coll($collId);
        $db = new Database();
        $query = 'SELECT res_id FROM ' . $table . ' WHERE res_id = ?';
        $stmt = $db->query($query, array($resId), true);
        if ($stmt->rowCount() == 0) {
            $returnCode = -2;
            $error .= 'res_id inexistant';
        } else {
            $resAttach = new resource();
            $_SESSION['data'] = array();
            array_push(
                $_SESSION['data'],
                array(
                    'column' => "typist",
                    'value' => $_SESSION['user']['UserId'],
                    'type' => "string",
                )
            );
            array_push(
                $_SESSION['data'],
                array(
                    'column' => "format",
                    'value' => strtolower($data['format']),
                    'type' => "string",
                )
            );
            array_push(
                $_SESSION['data'],
                array(
                    'column' => "docserver_id",
                    'value' => $data['docserver_id'],
                    'type' => "string",
                )
            );
            array_push(
                $_SESSION['data'],
                array(
                    'column' => "status",
                    'value' => $data['status'],
                    'type' => "string",
                )
            );
            array_push(
                $_SESSION['data'],
                array(
                    'column' => "offset_doc",
                    'value' => ' ',
                    'type' => "string",
                )
            );
            array_push(
                $_SESSION['data'],
                array(
                    'column' => "logical_adr",
                    'value' => ' ',
                    'type' => "string",
                )
            );
            array_push(
                $_SESSION['data'],
                array(
                    'column' => "title",
                    'value' => strtolower($data['subject']),
                    'type' => "string",
                )
            );
            array_push(
                $_SESSION['data'],
                array(
                    'column' => "coll_id",
                    'value' => $collId,
                    'type' => "string",
                )
            );
            array_push(
                $_SESSION['data'],
                array(
                    'column' => "res_id_master",
                    'value' => $resId,
                    'type' => "integer",
                )
            );
            array_push(
                $_SESSION['data'],
                array(
                    'column' => "relation",
                    'value' => $data['relation'],
                    'type' => "integer",
                )
            );
            array_push(
                $_SESSION['data'],
                array(
                    'column' => "origin",
                    'value' => $data['origin'],
                    'type' => "string",
                )
            );
            array_push(
                $_SESSION['data'],
                array(
                    'column' => "identifier",
                    'value' => $data['identifier'],
                    'type' => "string",
                )
            );
            array_push(
                $_SESSION['data'],
                array(
                    'column' => "attachment_type",
                    'value' => $data['attachment_type'],
                    'type' => "string",
                )
            );
            array_push(
                $_SESSION['data'],
                array(
                    'column' => "dest_contact_id",
                    'value' => $data['dest_contact_id'],
                    'type' => "string",
                )
            );
            array_push(
                $_SESSION['data'],
                array(
                    'column' => "dest_address_id",
                    'value' => $data['dest_address_id'],
                    'type' => "string",
                )
            );
            if ($_SESSION['origin'] == "scan") {
                array_push(
                    $_SESSION['data'],
                    array(
                        'column' => "scan_user",
                        'value' => $_SESSION['user']['UserId'],
                        'type' => "string",
                    )
                );
                array_push(
                    $_SESSION['data'],
                    array(
                        'column' => "scan_date",
                        'value' => $req->current_datetime(),
                        'type' => "function",
                    )
                );
            }
            array_push(
                $_SESSION['data'],
                array(
                    'column' => "type_id",
                    'value' => 0,
                    'type' => "int",
                )
            );
            $id = $resAttach->load_into_db(
                $_SESSION['tablename']['reconciliation']['attachment'],
                $data['path'],
                $data['filename'],
                $data['path_template'],
                $data['docserver_id'],
                $_SESSION['data'],
                $_SESSION['config']['databasetype']
            );
	        if ($id == false) {
                $returnCode = -6;
                $error = $resAttach->get_error();
            } else {
                $returnCode = 0;
                if ($_SESSION['history']['attachadd'] == "true") {
                    $users = new history();
                    $view = $sec->retrieve_view_from_coll_id(
                        $collId
                    );
                    $users->add(
                        $view, $resId, "ADD", 'attachadd',
                        ucfirst(_DOC_NUM) . $id . ' '
                        . _NEW_ATTACH_ADDED . ' ' . _TO_MASTER_DOCUMENT
                        . $resId,
                        $_SESSION['config']['databasetype'],
                        'apps'
                    );
                    $users->add(
                        RES_ATTACHMENTS_TABLE, $id, "ADD", 'attachadd',
                        _NEW_ATTACH_ADDED . " (" . $data['subject']
                        . ") ",
                        $_SESSION['config']['databasetype'],
                        'attachments'
                    );
                }
            }
        }
        $returnArray = array(
            'returnCode' => (int) $returnCode,
            'resId' => $id,
            'error' => $error,
        );
        return $returnArray;
    }

    function get_values_in_array($val){
        $tab = explode('$$',$val);
        $values = array();
        for($i=0; $i<count($tab);$i++)
        {
            $tmp = explode('#', $tab[$i]);

            $val_tmp=array();
            for($idiese=1;$idiese<count($tmp);$idiese++){
                $val_tmp[]=$tmp[$idiese];
            }
            $valeurDiese = implode("#",$val_tmp);
            if(isset($tmp[1]))
            {
                array_push($values, array('ID' => $tmp[0], 'VALUE' => $valeurDiese));
            }
        }
        return $values;
    }
}