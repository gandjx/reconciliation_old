<?php

/*
*   Copyright 2008-2015 Maarch
*
*   This file is part of Maarch Framework.
*
*   Maarch Framework is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   Maarch Framework is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with Maarch Framework.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * modules tools Class for reconciliation
 *
 *  Contains all the functions to load modules tables for reconciliation
 *
 * @package  maarch
 * @version 3.0
 * @since 10/2005
 * @license GPL v3
 * @author  <dev@maarch.org>
 *
 */

abstract class reconciliation_Abstract
{
    /**
     * Build Maarch module tables into sessions vars with a xml configuration
     * file
     */
    public function build_modules_tables()
    {
        if (file_exists(
            $_SESSION['config']['corepath'] . 'custom' . DIRECTORY_SEPARATOR
            . $_SESSION['custom_override_id'] . DIRECTORY_SEPARATOR . "modules"
            . DIRECTORY_SEPARATOR . "reconciliation" . DIRECTORY_SEPARATOR . "xml"
            . DIRECTORY_SEPARATOR . "config.xml"
        )
        ) {
            $configPath = $_SESSION['config']['corepath'] . 'custom'
                . DIRECTORY_SEPARATOR . $_SESSION['custom_override_id']
                . DIRECTORY_SEPARATOR . "modules" . DIRECTORY_SEPARATOR
                . "reconciliation" . DIRECTORY_SEPARATOR . "xml"
                . DIRECTORY_SEPARATOR . "config.xml";
        } else {
            $configPath = "modules" . DIRECTORY_SEPARATOR . "reconciliation"
                . DIRECTORY_SEPARATOR . "xml" . DIRECTORY_SEPARATOR
                . "config.xml";
        }
        $xmlconfig = simplexml_load_file($configPath);
        foreach ($xmlconfig->TABLENAME as $tableName) {
            $_SESSION['tablename']['reconciliation']['attachment'] = (string) $tableName -> attachment;
            $_SESSION['tablename']['reconciliation']['letterbox'] = (string) $tableName -> letterbox;
            $_SESSION['tablename']['reconciliation']['contacts_v2'] = (string) $tableName -> contacts_v2;
            $_SESSION['tablename']['reconciliation']['contact_addresses'] = (string) $tableName -> contact_addresses;
        }
        $conf = $xmlconfig->CONFIG;
        $_SESSION['modules_loaded']['reconciliation']['multiple_res_id'] = (string) $conf -> multiple_res_id;
        $_SESSION['modules_loaded']['reconciliation']['attach_to_empty'] = (string) $conf -> attach_to_empty;
        $_SESSION['modules_loaded']['reconciliation']['delete_response_project'] = (string) $conf -> delete_response_project;
    }
}